﻿using if3250_2022_05_gerbang_be.Data;
using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace if3250_2022_05_gerbang_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private ICategoryService categoryService;

        public CategoryController(ICategoryService _categoryService)
        {
            categoryService = _categoryService;
        }

        [HttpGet]
        public async Task<List<Category>> Get()
        {

            var categories = await categoryService.Get();
            return categories;
        }

        [HttpGet("{product_type_id}")]
        public async Task<List<Category>> Get(int product_type_id)
        {

            var categories = await categoryService.Get(product_type_id);
            return categories;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post([FromBody] Category category)
        {
            try
            {
                var id = await categoryService.Post(category);
                if (id == -1)
                {
                    return BadRequest("Gagal menambahkan data");
                }
                return (id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put(Category category)
        {
            try
            {
                await categoryService.Put(category);

                return Ok("Kategori Berhasil Diperbaharui");
            }

            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await categoryService.Delete(id);

                return Ok("Kategori berhasil dihapus");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
    }

}
}
