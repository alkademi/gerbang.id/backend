﻿using if3250_2022_05_gerbang_be.Data;
using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using static if3250_2022_05_gerbang_be.Models.Product;
using static if3250_2022_05_gerbang_be.Services.ProductService;

namespace if3250_2022_05_gerbang_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IProductService productService;

        public ProductController(IProductService _productService)
        {
            productService = _productService;
        }

        [HttpGet]
        public async Task<List<Product>> Get(
            [FromQuery(Name = "name")] string ProductName,
            [FromQuery(Name = "age")] ages? age,
            [FromQuery(Name = "category")] int? category_id,
            [FromQuery(Name = "type")] int? type_id,
            [FromQuery(Name ="tab")] tab_options? selectedTab,
            [FromQuery(Name ="sort_by")] sort_options? sortBy,
            [FromQuery(Name = "asc")] bool isAscending,
            [FromQuery(Name ="limit")] int? limit)
        {
            var products = await productService.Get(ProductName,age,category_id,type_id,selectedTab,sortBy,isAscending,limit);
              
            return products;
        }

        [HttpGet("{id}")]
        public async Task<List<Product>> Get(int id)
        {
            var products = await productService.Get(id);

            return products;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post(Product Product)
        {
            try
            {
                var id = await productService.Post(Product);

                if (id == -1)
                {
                    return BadRequest("Gagal menambahkan data");
                }
                return (id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put(Product Product)
        {
            try
            {
                await productService.Put(Product);

                return Ok("Detail Produk Berhasil Diperbaharui");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await productService.Delete(id);

                return Ok("Produk berhasil dihapus");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }
    }
}
