﻿using if3250_2022_05_gerbang_be.Data;
using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using static if3250_2022_05_gerbang_be.Models.ProductReviews;
using static if3250_2022_05_gerbang_be.Services.ProductReviewsService;

namespace if3250_2022_05_gerbang_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductReviewsController : ControllerBase
    {
        private IProductReviewsService productReviewsService;

        public ProductReviewsController(IProductReviewsService _productReviewsService)
        {
            productReviewsService = _productReviewsService;
        }

        [HttpGet]
        public async Task<List<ProductReviews>> Get(
            [FromQuery(Name = "review_id")] int? review_id,
            [FromQuery(Name = "product_id")] int? product_id,
            [FromQuery(Name = "user_id")] int? user_id,
            [FromQuery(Name = "stars")] int? stars,
            [FromQuery(Name = "review_date")] DateTime? review_date,
            [FromQuery(Name = "review_text")] string review_text,
            [FromQuery(Name = "sort_by")] review_sort_opt? sort_by
            )
        {

            var productReviews = await productReviewsService.Get(
                review_id,
                product_id,
                user_id,
                stars,
                review_date,
                review_text,
                sort_by);
               
            return productReviews;
        }

        [HttpGet("{id}")]
        public async Task<List<ProductReviews>> Get(int id)
        {
            var productReviews = await productReviewsService.Get(id);

            return productReviews;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post(ProductReviews ProductRev)
        {
            try
            {
                var id = await productReviewsService.Post(ProductRev);
                if (id == -1)
                {
                    return BadRequest("Review Berhasil Ditambahkan");
                }
                return id;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put(
            [FromQuery(Name = "review_id")] int review_id,
            [FromQuery(Name = "product_id")] int? product_id,
            [FromQuery(Name = "user_id")] int? user_id,
            [FromQuery(Name = "stars")] int stars,
            [FromQuery(Name = "review_date")] DateTime? review_date,
            [FromQuery(Name = "review_text")] string review_text)
        {
            try
            {
                await productReviewsService.Put(
                    review_id,
                    product_id,
                    user_id,
                    stars,
                    review_date,
                    review_text);

                return Ok("Review berhasil di-update");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await productReviewsService.Delete(id);

                return Ok("Review berhasil dihapus");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
    }
}

