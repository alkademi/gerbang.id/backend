﻿using if3250_2022_05_gerbang_be.Data;
using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace if3250_2022_05_gerbang_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductTypesController : ControllerBase
    {
        private IProductTypesService productTypesService;

        public ProductTypesController(IProductTypesService _productTypesService)
        {
            productTypesService = _productTypesService;
        }

        [HttpGet]
        public async Task<List<ProductTypes>> Get()
        {

            var productTypes = await productTypesService.Get();

            return productTypes;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post(ProductTypes prod_type)
        {
            try
            {
                var id = await productTypesService.Post(prod_type);

                if (id == -1)
                {
                    return BadRequest("Tipe Produk Berhasil Ditambahkan");
                }
                return id;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put(ProductTypes prod_type)
        {
            try
            {
                await productTypesService.Put(prod_type);

                return Ok("Tipe Produk Berhasil Diperbaharui");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await productTypesService.Delete(id);

                return Ok("Tipe produk berhasil dihapus");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
    }
}
