﻿using if3250_2022_05_gerbang_be.Data;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using Npgsql;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace if3250_2022_05_gerbang_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService userService;

        public UsersController(IUserService _userService)
        {
            userService = _userService;
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] UserData user)
        {
            try
            {
                await userService.register(user);
                return Ok($"Registrasi berhasil.");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpGet]
        // GET
        // endpoint + query : /api/Users?user=username
        // Authorization header -> Bearer token
        // response : {fullName,dateOfBirth,email,userName}
        public async Task<IActionResult> GetAccountInfo([FromQuery] string user)
        {
            try
            {
                var tup = await userService.getAccountInfo(user);
                var accountInfo = new { tup.FullName, tup.DateOfBirth, tup.Email, tup.UserName };
                return Ok(accountInfo);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("role")]
        public async Task<IActionResult> GetAccountRole([FromQuery] string user)
        {
            try
            {
                var table = await userService.GetAccountRole(user);

                return new JsonResult(table);
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
        [HttpGet("username")]
        public async Task<IActionResult> GetUsername([FromQuery] string email)
        {
            try
            {
                var table = await userService.GetUsername(email);

                return new JsonResult(table);
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> Authentication([FromBody] UserLoginData userData)
        {
            var user = await userService.authenticate(userData.Username,userData.Password, ipAddress());
            if (user == null)
                return BadRequest("Username/Password incorrect");
            //setTokenCookie(user.RefreshToken);
            return Ok(user);
        }

        private string ipAddress()
        {
            // get source ip address for the current request
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }

        private void setTokenCookie(string token)
        {
            // append cookie with refresh token to the http response
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddDays(7)
            };
            Response.Cookies.Append("refreshToken", token, cookieOptions);
        }
    }
}
