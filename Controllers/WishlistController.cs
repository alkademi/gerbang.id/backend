﻿using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace if3250_2022_05_gerbang_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WishlistController : ControllerBase
    {
        private IWishlistService wishlistService;

        public WishlistController(IWishlistService _wishlistService)
        {
            wishlistService = _wishlistService;
        }

        [HttpGet]
        public async Task<List<Wishlist>> Get()
        {
            var wishlist = await wishlistService.Get();

            return wishlist;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post(Wishlist wishlist)
        {
            try
            {
                var id = await wishlistService.Post(wishlist);

                if (id == -1)
                {
                    return BadRequest("Wishlist Berhasil Ditambahkan");
                }
                return id;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpPut]
        public async Task<IActionResult> Put(Wishlist wishlist)
        {
            try
            {
                await wishlistService.Put(wishlist);

                return Ok("Wishlist Berhasil Diperbaharui");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await wishlistService.Delete(id);

                return Ok("Wishlist berhasil dihapus");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }
    }
}
