﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace if3250_2022_05_gerbang_be.Data
{
    public class UserLoginData
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
