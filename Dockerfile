#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["if3250_2022_05_gerbang_be.csproj", "."]
RUN dotnet restore "./if3250_2022_05_gerbang_be.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "if3250_2022_05_gerbang_be.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "if3250_2022_05_gerbang_be.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "if3250_2022_05_gerbang_be.dll"]
