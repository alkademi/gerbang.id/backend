﻿using Ensiklopedia46.Data;
using if3250_2022_05_gerbang_be.Data;
using if3250_2022_05_gerbang_be.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace if3250_2022_05_gerbang_be.Helpers
{
    public class JWTHelper : JWTHelperInt
    {
        private GerbangContext _db;
        private AppSettings _appSettings;

        public JWTHelper(GerbangContext db, IOptions<AppSettings> appSettings )
        {
            _db = db;
            _appSettings = appSettings.Value;
        }

        public string generateJwtToken(string username)
        {
            //MEMBUAT JWT TOKEN
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, username));

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(2), /*TOKEN DURATION*/
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public RefreshToken generateRefreshToken(string ipAddress)
        {
            // generate token that is valid for 1 days
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[64];
            rngCryptoServiceProvider.GetBytes(randomBytes);
            var refreshToken = new RefreshToken
            {
                Token = Convert.ToBase64String(randomBytes),
                Expires = DateTime.UtcNow.AddHours(8),/*REFRESH TOKEN DURATION*/
                Created = DateTime.UtcNow,
                CreatedByIp = ipAddress
            };

            return refreshToken;
        }

        public async Task removeOldRefreshToken(AspNetUser user)
        {
            try
            {
                var items = await _db.RefreshTokens.Where(i => i.UserId == user.Id).ToListAsync();
                foreach (var item in items)
                {
                    if (DateTime.Compare(item.Expires, DateTime.UtcNow) < 0)
                    {
                        _db.RefreshTokens.Remove(item);
                    }
                }
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                throw new Exception($"Database Error: {e.Message}");
            }
            catch (Exception e)
            {
                throw new Exception($"Error: {e.Message}");
            }
        }

    }
}
