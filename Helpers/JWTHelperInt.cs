﻿using if3250_2022_05_gerbang_be.Data;
using if3250_2022_05_gerbang_be.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace if3250_2022_05_gerbang_be.Helpers
{
    public interface JWTHelperInt
    {
        public string generateJwtToken(string username);
        public RefreshToken generateRefreshToken(string ipAddress);
        public Task removeOldRefreshToken(AspNetUser user);
    }
}
