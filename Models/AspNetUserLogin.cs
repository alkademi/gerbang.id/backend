﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

#nullable disable

namespace if3250_2022_05_gerbang_be.Models
{
    public partial class AspNetUserLogin : IdentityUserLogin<int>
    {
        public virtual AspNetUser User { get; set; }
    }
}
