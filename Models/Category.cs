﻿using System;
namespace if3250_2022_05_gerbang_be.Models
{
    public class Category
    {
        public int CategoryId { get; set; }

        public int ProductTypesId { get; set; }

        public string CategoryName { get; set; }
    }
}
