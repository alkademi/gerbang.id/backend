﻿using System;
namespace if3250_2022_05_gerbang_be.Models
{
    public class CategoryRequested
    {
        public int ProductTypesId { get; set; }

        public string CategoryName { get; set; }
    }
}
