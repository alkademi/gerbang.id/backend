﻿using System;
namespace if3250_2022_05_gerbang_be.Models
{
    public class Product
    {
        public enum size_units
        {
            kb,
            mb,
            gb
        }
        public enum ages
        {
            child,
            teen,
            adult
        }
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public int ProductTypesId { get; set; }

        public int ProductCategoryId { get; set; }

        public string ProductPublisher { get; set; }

        public float ProductRating { get; set; }

        public float ProductSize { get; set; }

        public size_units ProductSizeUnit { get; set; }

        public ages ProductAgeRestriction { get; set; }

        public string ProductDesc { get; set; }

        public string ProductLogo { get; set; }

        public string ProductLogoName { get; set; }

        public string[] ProductImages { get; set; }

        public string[] ProductImagesName { get; set; }
    }
}
