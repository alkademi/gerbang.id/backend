﻿using System;
using System.Collections.Generic;
namespace if3250_2022_05_gerbang_be.Models
{
    public class ProductReviews
    {
        public int ReviewId { get; set; }
        public int ProductId { get; set; }
        public int UserId { get; set; }
        public int Stars { get; set; }

        public DateTime? ReviewDate { get; set; }
        public string ReviewText { get; set; }

        public string UserName { get; set; }
    }
}
