﻿using System;
namespace if3250_2022_05_gerbang_be.Models
{
    public class ProductTypes
    {
        public int ProductTypesId { get; set; }

        public string ProductTypesName { get; set; }
    }
}
