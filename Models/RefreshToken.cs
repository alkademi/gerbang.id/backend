﻿using System;
using System.Collections.Generic;

#nullable disable

namespace if3250_2022_05_gerbang_be.Models
{
    public partial class RefreshToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime Expires { get; set; }
        public DateTime Created { get; set; }
        public string CreatedByIp { get; set; }
        public DateTime? Revoked { get; set; }
        public string RevokedByIp { get; set; }
        public string ReplacedByToken { get; set; }
        public string ReasonRevoked { get; set; }
        public bool? IsRevoked { get; set; }
        public int UserId { get; set; }

        public virtual AspNetUser User { get; set; }
    }
}
