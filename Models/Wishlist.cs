﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace if3250_2022_05_gerbang_be.Models
{
    public class Wishlist
    {
        public int id_wishlist { get; set; }
        public int id_product { get; set; }
        public string username { get; set; }
    }
}
