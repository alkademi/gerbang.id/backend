﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using if3250_2022_05_gerbang_be.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace if3250_2022_05_gerbang_be.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IConfiguration _configuration;
        private GerbangContext _db;

        public CategoryService(IConfiguration configuration, GerbangContext db)
        {
            _configuration = configuration;
            _db = db;
        }

        public async Task<List<Category>> Get()
        {
            string query = @"
                select ""CategoryId"" as ""CategoryId"",
                        ""ProductTypesId"" as ""ProductTypesId"",
                        ""CategoryName"" as ""CategoryName""
                from ""Categories""
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                }
            }

            var categories = (from rw in table.AsEnumerable()
                                 select new Category()
                                 {
                                     CategoryId = Convert.ToInt32(rw["CategoryId"]),
                                     CategoryName = Convert.ToString(rw["CategoryName"]),
                                     ProductTypesId = Convert.ToInt32(rw["ProductTypesId"])
                                 }).ToList();

            return categories;
        }

        public async Task<List<Category>> Get(int product_type_id)
        {
            string query = @"
                select ""CategoryId"" as ""CategoryId"",
                        ""CategoryName"" as ""CategoryName""
                from ""Categories""
                where ""ProductTypesId""=@ProductTypesId
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductTypesId", product_type_id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }

            var categories = (from rw in table.AsEnumerable()
                              select new Category()
                              {
                                  CategoryId = Convert.ToInt32(rw["CategoryId"]),
                                  CategoryName = Convert.ToString(rw["CategoryName"]),
                              }).ToList();

            return categories;
        }

        public async Task<int> Post(Category category)
        {
            int id = -1;

            string query = @"
                insert into ""Categories""(""ProductTypesId"",""CategoryName"")
                values (@ProductTypesId, @CategoryName) RETURNING ""CategoryId"";
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductTypesId", category.ProductTypesId);
                    myCommand.Parameters.AddWithValue("@CategoryName", category.CategoryName);
                    object returnObj = myCommand.ExecuteScalar();

                    if (returnObj != null)
                    {
                        int.TryParse(returnObj.ToString(), out id);
                    }
                }
            }

            return id;
        }

        public async Task Put(Category category)
        {
            string query = @"
                update ""Categories""
                set ""CategoryName""=@CategoryName
                where ""CategoryId""=@CategoryId 
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@CategoryId", category.CategoryId);
                    myCommand.Parameters.AddWithValue("@CategoryName", category.CategoryName);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }
        }

        public async Task<DataTable> Delete(int id)
        {
            string query = @"
                delete from ""Categories""
                where ""CategoryId""=@CategoryId 
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@CategoryId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }

            return table;
        }

    }
}
