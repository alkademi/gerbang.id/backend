﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using if3250_2022_05_gerbang_be.Models;

namespace if3250_2022_05_gerbang_be.Services
{
    public interface ICategoryService
    {
        public Task<List<Category>> Get();
        public Task<List<Category>> Get(int product_type_id);
        public Task<int> Post(Category category);
        public Task Put(Category category);
        public Task<DataTable> Delete(int id);
    }
}
