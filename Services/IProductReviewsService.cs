﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using if3250_2022_05_gerbang_be.Models;
using static if3250_2022_05_gerbang_be.Services.ProductReviewsService;

namespace if3250_2022_05_gerbang_be.Services
{
    public interface IProductReviewsService
    {
        public Task<List<ProductReviews>> Get(int? review_id, int? product_id, int? user_id, int? stars, DateTime? review_date, string review_text, review_sort_opt? sort_by);
        public Task<List<ProductReviews>> Get(int id);
        public Task<int> Post(ProductReviews ProductRev);
        public Task Put(int review_id, int? product_id, int? user_id, int stars, DateTime? review_date, string review_text);
        public Task Delete(int id);
    }
}
