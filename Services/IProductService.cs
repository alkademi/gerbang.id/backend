﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using if3250_2022_05_gerbang_be.Models;
using static if3250_2022_05_gerbang_be.Models.Product;
using static if3250_2022_05_gerbang_be.Services.ProductService;

namespace if3250_2022_05_gerbang_be.Services
{
    public interface IProductService
    {
        public Task<List<Product>> Get(string ProductName, ages? age, int? category_id, int? type_id, tab_options? selectedTab, sort_options? sortBy, bool isAscending, int? limit);
        public Task<List<Product>> Get(int id);
        public Task<int> Post(Product Product);
        public Task Put(Product Product);
        public Task Delete(int id);
    }
}
