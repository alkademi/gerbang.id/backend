﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using if3250_2022_05_gerbang_be.Models;

namespace if3250_2022_05_gerbang_be.Services
{
    public interface IProductTypesService
    {
        public Task<List<ProductTypes>> Get();
        public Task<int> Post(ProductTypes prod_type);
        public Task Put(ProductTypes prod_type);
        public Task Delete(int id);
    }
}
