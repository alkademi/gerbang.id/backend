﻿using if3250_2022_05_gerbang_be.Data;
using if3250_2022_05_gerbang_be.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace if3250_2022_05_gerbang_be.Services
{
    public interface IUserService
    {
        public Task register(UserData user);
        public Task<AspNetUser> getAccountInfo(string user);
        public Task<TokenResponse> authenticate(string username, string password, string ipAddress);
        public Task<DataTable> GetAccountRole(string user);
        public Task<DataTable> GetUsername(string email);
    }

}
