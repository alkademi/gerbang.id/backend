﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using if3250_2022_05_gerbang_be.Models;

namespace if3250_2022_05_gerbang_be.Services
{
    public interface IWishlistService
    {
        public Task<List<Wishlist>> Get();
        public Task<int> Post(Wishlist wishlist);
        public Task Put(Wishlist wishlist);
        public Task Delete(int id);
    }
}
