﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data;
using Npgsql;
using if3250_2022_05_gerbang_be.Models;
using System.Linq;
using System.Collections.Generic;

namespace if3250_2022_05_gerbang_be.Services
{
    public class ProductReviewsService : IProductReviewsService
    {
        private readonly IConfiguration _configuration;

        public ProductReviewsService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public enum review_sort_opt
        {
            stars, date
        }

        public async Task<List<ProductReviews>> Get(int? review_id, int? product_id, int? user_id, int? stars, DateTime? review_date, string review_text, review_sort_opt? sort_by)
        {
            string query = @"
            select ""ReviewId"" as ""ReviewId"",
                    ""ProductId"" as ""ProductId"",
                    ""ProductReviews"".""UserId"" as ""UserId"",
                    ""Stars"" as ""Stars"",
                    ""ReviewDate"" as ""ReviewDate"",
                    ""ReviewText"" as ""ReviewText"",
                    ""UserName"" as ""UserName""

            from ""ProductReviews"", ""AspNetUsers""
            where true AND ""ProductReviews"".""UserId"" = ""AspNetUsers"".""Id""";

            if (review_text != null)
            {
                query += @" AND ""ReviewText"" ILIKE @AReviewText";
            }

            if (review_id != null)
            {
                query += @" AND ""ReviewId"" = @AReviewId";
            }
            if (product_id != null)
            {
                query += @" AND ""ProductId"" = @AProductId";
            }
            if (user_id != null)
            {
                query += @" AND ""UserId"" = @AUserId";
            }

            if (stars != null)
            {
                query += @" AND ""Stars"" = @AStars";

            }
            if (review_date != null)
            {
                query += @" AND ""ReviewDate"" = @AReviewDate";
            }
            if (sort_by != null)
            {
                query += @" ORDER BY";

                if (sort_by == review_sort_opt.stars) //0
                {
                    query += @" ""Stars""";
                }
                else if (sort_by == review_sort_opt.date) // 1
                {
                    query += @" ""ReviewDate""";
                }
                query += @" DESC";
            }
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    if (review_id != null) myCommand.Parameters.AddWithValue("@AReviewId", review_id);
                    if (product_id != null) myCommand.Parameters.AddWithValue("@AProductId", product_id);
                    if (user_id != null) myCommand.Parameters.AddWithValue("@AUserId", user_id);
                    if (stars != null) myCommand.Parameters.AddWithValue("@AStars", stars);
                    if (review_date != null) myCommand.Parameters.AddWithValue("@AReviewDate", review_date);
                    if (review_text != null) myCommand.Parameters.AddWithValue("@AReviewText", $"%{review_text}%");
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }

            var productReviews = (from rw in table.AsEnumerable()
                              select new ProductReviews()
                              {
                                  ReviewId = Convert.ToInt32(rw["ReviewId"]),
                                  ProductId = Convert.ToInt32(rw["ProductId"]),
                                  UserId = Convert.ToInt32(rw["UserId"]),
                                  Stars = Convert.ToInt32(rw["Stars"]),
                                  ReviewDate = Convert.ToDateTime(rw["ReviewDate"]),
                                  ReviewText = Convert.ToString(rw["ReviewText"]),
                                  UserName = Convert.ToString(rw["UserName"])
                              }).ToList();

            return productReviews;

        }

        public async Task<List<ProductReviews>> Get(int id)
        {
            string query = @"
            select ""ReviewId"" as ""ReviewId"",
                    ""ProductId"" as ""ProductId"",
                    ""UserId"" as ""UserId"",
                    ""Stars"" as ""Stars"",
                    ""ReviewDate"" as ""ReviewDate"",
                    ""ReviewText"" as ""ReviewText""
            from ""ProductReviews""
            where ""ReviewId""=@ReviewId";


            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ReviewId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }

            var productReviews = (from rw in table.AsEnumerable()
                                  select new ProductReviews()
                                  {
                                      ReviewId = Convert.ToInt32(rw["ReviewId"]),
                                      ProductId = Convert.ToInt32(rw["ProductId"]),
                                      UserId = Convert.ToInt32(rw["UserId"]),
                                      Stars = Convert.ToInt32(rw["Stars"]),
                                      ReviewDate = Convert.ToDateTime(rw["ReviewDate"]),
                                      ReviewText = Convert.ToString(rw["ReviewText"])
                                  }).ToList();

            return productReviews;
        }

        public async Task<int> Post(ProductReviews ProductRev)
        {
            int id = -1;
            string query = @"
            insert into ""ProductReviews""(""ProductId"",""UserId"",""Stars"",""ReviewDate"",""ReviewText"")
            values (@ProductId,@UserId,@Stars,@ReviewDate,@ReviewText) RETURNING ""ReviewId""
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductId", ProductRev.ProductId);
                    myCommand.Parameters.AddWithValue("@UserId", ProductRev.UserId);
                    myCommand.Parameters.AddWithValue("@Stars", ProductRev.Stars);
                    myCommand.Parameters.AddWithValue("@ReviewDate", ProductRev.ReviewDate);
                    myCommand.Parameters.AddWithValue("@ReviewText", ProductRev.ReviewText);
                    object returnObj = myCommand.ExecuteScalar();

                    if (returnObj != null)
                    {
                        int.TryParse(returnObj.ToString(), out id);
                    }
                }
            }
            return id;
        }

        public async Task Put(int review_id, int? product_id, int? user_id, int stars, DateTime? review_date, string review_text)
        {
            string query = @"
            update ""ProductReviews""
            set ""Stars""= @AStars,
                ""ReviewText"" = @AReviewText";

            if (user_id != null)
            {
                query += @" ,""UserId"" = @AUserId";
            }

            if (product_id != null)
            {
                query += @" ,""ProductId"" = @AProductId";

            }
            if (review_date != null)
            {
                query += @" ,""ReviewDate"" = @AReviewDate";
            }

            query += @" where ""ReviewId"" = @AReviewId";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@AReviewId", review_id);
                    if (product_id != null) myCommand.Parameters.AddWithValue("@AProductId", product_id);
                    if (user_id != null) myCommand.Parameters.AddWithValue("@AUserId", user_id);
                    myCommand.Parameters.AddWithValue("@AStars", stars);
                    if (review_date != null) myCommand.Parameters.AddWithValue("@AReviewDate", review_date);
                    myCommand.Parameters.AddWithValue("@AReviewText", review_text);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }

        }

        public async Task Delete(int id)
        {
            string query = @"
                delete from ""ProductReviews""
                where ""ReviewId""=@ReviewId 
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ReviewId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }
        }
    }
}
