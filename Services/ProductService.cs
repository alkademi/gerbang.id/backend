﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using if3250_2022_05_gerbang_be.Models;
using Microsoft.Extensions.Configuration;
using static if3250_2022_05_gerbang_be.Models.Product;
using System.Data;
using Npgsql;
using System.Linq;

namespace if3250_2022_05_gerbang_be.Services
{
    public class ProductService : IProductService
    {
        private readonly IConfiguration _configuration;

        public ProductService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public enum sort_options
        {
            id, name, rating
        }

        public enum tab_options
        {
            untuk_anda, populer, anak_anak
        }

        public async Task<List<Product>> Get(string ProductName, ages? age, int? category_id, int? type_id, tab_options? selectedTab, sort_options? sortBy, bool isAscending, int? limit)
        {
            bool tab_sort = false;

            string query = @"
            select ""ProductId"" as ""ProductId"",
                    ""ProductName"" as ""ProductName"",
                    ""ProductTypesId"" as ""ProductTypesId"",
                    ""ProductCategoryId"" as ""ProductCategoryId"",
                    ""ProductPublisher"" as ""ProductPublisher"",
                    ""ProductRating"" as ""ProductRating"",
                    ""ProductSize"" as ""ProductSize"",
                    ""ProductSizeUnit"" as ""ProductSizeUnit"",
                    ""ProductAgeRestriction"" as ""ProductAgeRestriction"",
                    ""ProductDesc"" as ""ProductDesc"",
                    ""ProductLogo"" as ""ProductLogo"",
                    ""ProductLogoName"" as ""ProductLogoName"",
                    ""ProductImages"" as ""ProductImages"",
                    ""ProductImagesName"" as ""ProductImagesName""
            from ""Products""
            where true";

            if (ProductName != null)
            {
                query += @" AND ""ProductName"" ILIKE @PName";
            }

            if (age != null)
            {
                query += @" AND ""ProductAgeRestriction"" = @PAgeRestriction";
            }

            if (category_id != null)
            {
                query += @" AND ""ProductCategoryId"" = @PCategory";
            }

            if (type_id != null)
            {
                query += @" AND ""ProductTypesId"" = @PType";

            }
            if (selectedTab != null)
            {
                if (selectedTab == tab_options.anak_anak)
                {
                    query += @" AND ""ProductAgeRestriction"" = 'child'";
                }
            }
            if (sortBy != null)
            {
                query += @" ORDER BY";
                tab_sort = true;

                if (sortBy == sort_options.rating) //2
                {
                    query += @" ""ProductRating""";
                }
                else if (sortBy == sort_options.name) // 1
                {
                    query += @" ""ProductName""";
                }
                else //0
                {
                    query += @" ""ProductId""";
                }
                query += isAscending ? @" ASC" : @" DESC";
            }

            if (selectedTab != null)
            {

                if (selectedTab == tab_options.untuk_anda)
                {
                    query += tab_sort ? @" ," : @" ORDER BY";
                    query += @" ""ProductId"" DESC";
                }
                else if (selectedTab == tab_options.populer)
                {
                    query += tab_sort ? @" ," : @" ORDER BY";
                    query += @" ""ProductRating"" DESC";
                }
            }

            if (limit != null && limit > 0)
            {
                query += @" LIMIT @Limit_num";
            }

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    if (ProductName != null)
                    {
                        myCommand.Parameters.AddWithValue("@PName", $"%{ProductName}%");
                    }
                    if (age != null)
                    {
                        myCommand.Parameters.AddWithValue("@PAgeRestriction", age);
                    }
                    if (category_id != null)
                    {
                        myCommand.Parameters.AddWithValue("@PCategory", category_id);
                    }
                    if (type_id != null)
                    {
                        myCommand.Parameters.AddWithValue("@PType", type_id);
                    }
                    if (limit != null)
                    {
                        myCommand.Parameters.AddWithValue("@Limit_num", limit);
                    }
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }

            var product = (from rw in table.AsEnumerable()
                           select new Product()
                           {
                               ProductId = Convert.ToInt32(rw["ProductId"]),
                               ProductName = Convert.ToString(rw["ProductName"]),
                               ProductTypesId = Convert.ToInt32(rw["ProductTypesId"]),
                               ProductCategoryId = Convert.ToInt32(rw["ProductCategoryId"]),
                               ProductPublisher = Convert.ToString(rw["ProductPublisher"]),
                               ProductRating = (float)Convert.ToDouble(rw["ProductRating"]),
                               ProductSize = (float)Convert.ToDouble(rw["ProductSize"]),
                               ProductSizeUnit = (size_units)Enum.Parse(typeof(size_units), (string)rw["ProductSizeUnit"]),
                               ProductAgeRestriction = (ages)Enum.Parse(typeof(ages), (string)rw["ProductAgeRestriction"]),
                               ProductDesc = Convert.ToString(rw["ProductDesc"]),
                               ProductLogo = Convert.ToString(rw["ProductLogo"]),
                               ProductLogoName = Convert.ToString(rw["ProductLogoName"]),
                               ProductImages = (string[])rw["ProductImages"],
                               ProductImagesName = (string[])rw["ProductImagesName"]
                           }).ToList();

            return product;
        }

        public async Task<List<Product>> Get(int id)
        {
            string query = @"
            select ""ProductId"" as ""ProductId"",
                    ""ProductName"" as ""ProductName"",
                    ""ProductTypesId"" as ""ProductTypesId"",
                    ""ProductCategoryId"" as ""ProductCategoryId"",
                    ""ProductPublisher"" as ""ProductPublisher"",
                    ""ProductRating"" as ""ProductRating"",
                    ""ProductSize"" as ""ProductSize"",
                    ""ProductSizeUnit"" as ""ProductSizeUnit"",
                    ""ProductAgeRestriction"" as ""ProductAgeRestriction"",
                    ""ProductDesc"" as ""ProductDesc"",
                    ""ProductLogo"" as ""ProductLogo"",
                    ""ProductLogoName"" as ""ProductLogoName"",
                    ""ProductImages"" as ""ProductImages"",
                    ""ProductImagesName"" as ""ProductImagesName""
            from ""Products""
            where ""ProductId""=@ProductId
        ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }

            var product = (from rw in table.AsEnumerable()
                           select new Product()
                           {
                               ProductId = Convert.ToInt32(rw["ProductId"]),
                               ProductName = Convert.ToString(rw["ProductName"]),
                               ProductTypesId = Convert.ToInt32(rw["ProductTypesId"]),
                               ProductCategoryId = Convert.ToInt32(rw["ProductCategoryId"]),
                               ProductPublisher = Convert.ToString(rw["ProductPublisher"]),
                               ProductRating = (float)Convert.ToDouble(rw["ProductRating"]),
                               ProductSize = (float)Convert.ToDouble(rw["ProductSize"]),
                               ProductSizeUnit = (size_units)Enum.Parse(typeof(size_units), (string)rw["ProductSizeUnit"]),
                               ProductAgeRestriction = (ages)Enum.Parse(typeof(ages), (string)rw["ProductAgeRestriction"]),
                               ProductDesc = Convert.ToString(rw["ProductDesc"]),
                               ProductLogo = Convert.ToString(rw["ProductLogo"]),
                               ProductLogoName = Convert.ToString(rw["ProductLogoName"]),
                               ProductImages = (string[])rw["ProductImages"],
                               ProductImagesName = (string[])rw["ProductImagesName"]
                           }).ToList();

            return product;

        }

        public async Task<int> Post(Product Product)
        {
            int id = -1;
            string query = @"
            insert into ""Products""(""ProductName"",""ProductTypesId"",""ProductCategoryId"",""ProductPublisher"",""ProductRating"",""ProductSize"",""ProductSizeUnit"",""ProductAgeRestriction"",""ProductDesc"",""ProductLogo"",""ProductLogoName"",""ProductImages"",""ProductImagesName"")
            values (@ProductName,@ProductTypesId,@ProductCategoryId,@ProductPublisher,@ProductRating,@ProductSize,@ProductSizeUnit,@ProductAgeRestriction,@ProductDesc,@ProductLogo,@ProductLogoName,@ProductImages,@ProductImagesName)  RETURNING ""ProductId""
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductName", Product.ProductName);
                    myCommand.Parameters.AddWithValue("@ProductTypesId", Product.ProductTypesId);
                    myCommand.Parameters.AddWithValue("@ProductCategoryId", Product.ProductCategoryId);
                    myCommand.Parameters.AddWithValue("@ProductPublisher", Product.ProductPublisher);
                    myCommand.Parameters.AddWithValue("@ProductRating", Product.ProductRating);
                    myCommand.Parameters.AddWithValue("@ProductSize", Product.ProductSize);
                    myCommand.Parameters.AddWithValue("@ProductSizeUnit", Product.ProductSizeUnit);
                    myCommand.Parameters.AddWithValue("@ProductAgeRestriction", Product.ProductAgeRestriction);
                    myCommand.Parameters.AddWithValue("@ProductDesc", Product.ProductDesc);
                    myCommand.Parameters.AddWithValue("@ProductLogo", Product.ProductLogo);
                    myCommand.Parameters.AddWithValue("@ProductLogoName", Product.ProductLogoName);
                    myCommand.Parameters.AddWithValue("@ProductImages", Product.ProductImages);
                    myCommand.Parameters.AddWithValue("@ProductImagesName", Product.ProductImagesName);
                    object returnObj = myCommand.ExecuteScalar();

                    if (returnObj != null)
                    {
                        int.TryParse(returnObj.ToString(), out id);
                    }
                }
            }
            return id;
        }

        public async Task Put(Product Product)
        {
            string query = @"
            update ""Products""
            set ""ProductName""=@ProductName,
                ""ProductTypesId""=@ProductTypesId,
                ""ProductCategoryId""=@ProductCategoryId,
                ""ProductPublisher""=@ProductPublisher,
                ""ProductRating""=@ProductRating,
                ""ProductSize""=@ProductSize,
                ""ProductSizeUnit""=@ProductSizeUnit,
                ""ProductAgeRestriction""=@ProductAgeRestriction,
                ""ProductDesc""=@ProductDesc,
                ""ProductLogo""=@ProductLogo,
                ""ProductLogoName""=@ProductLogoName,
                ""ProductImages""=@ProductImages,
                ""ProductImagesName""=@ProductImagesName
                where ""ProductId""=@ProductId 
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductId", Product.ProductId);
                    myCommand.Parameters.AddWithValue("@ProductName", Product.ProductName);
                    myCommand.Parameters.AddWithValue("@ProductTypesId", Product.ProductTypesId);
                    myCommand.Parameters.AddWithValue("@ProductCategoryId", Product.ProductCategoryId);
                    myCommand.Parameters.AddWithValue("@ProductPublisher", Product.ProductPublisher);
                    myCommand.Parameters.AddWithValue("@ProductRating", Product.ProductRating);
                    myCommand.Parameters.AddWithValue("@ProductSize", Product.ProductSize);
                    myCommand.Parameters.AddWithValue("@ProductSizeUnit", Product.ProductSizeUnit);
                    myCommand.Parameters.AddWithValue("@ProductAgeRestriction", Product.ProductAgeRestriction);
                    myCommand.Parameters.AddWithValue("@ProductDesc", Product.ProductDesc);
                    myCommand.Parameters.AddWithValue("@ProductLogo", Product.ProductLogo);
                    myCommand.Parameters.AddWithValue("@ProductLogoName", Product.ProductLogoName);
                    myCommand.Parameters.AddWithValue("@ProductImages", Product.ProductImages);
                    myCommand.Parameters.AddWithValue("@ProductImagesName", Product.ProductImagesName);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }

        }

        public async Task Delete(int id)
        {
            string query = @"
                delete from ""Products""
                where ""ProductId""=@ProductId 
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }
        }
    }
}
