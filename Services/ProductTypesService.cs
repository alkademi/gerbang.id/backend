﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using if3250_2022_05_gerbang_be.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace if3250_2022_05_gerbang_be.Services
{
    public class ProductTypesService : IProductTypesService
    {
        private readonly IConfiguration _configuration;

        public ProductTypesService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<List<ProductTypes>> Get()
        {
            string query = @"
                select ""ProductTypesId"" as ""ProductTypesId"",
                        ""ProductTypesName"" as ""ProductTypesName""
                from ""ProductTypes""
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }

            var productTypes = (from rw in table.AsEnumerable()
                           select new ProductTypes()
                           {
                               ProductTypesId = Convert.ToInt32(rw["ProductTypesId"]),
                               ProductTypesName = Convert.ToString(rw["ProductTypesName"])
                           }).ToList();

            return productTypes;
        }

        public async Task<int> Post(ProductTypes prod_type)
        {
            int id = -1;
            string query = @"
                insert into ""ProductTypes""(""ProductTypesName"")
                values (@ProductTypesName)  RETURNING ""ProductTypesId""
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductTypesName", prod_type.ProductTypesName);
                    object returnObj = myCommand.ExecuteScalar();

                    if (returnObj != null)
                    {
                        int.TryParse(returnObj.ToString(), out id);
                    }
                }
            }
            return id;
        }

        public async Task Put(ProductTypes prod_type)
        {
            string query = @"
                update ""ProductTypes""
                set ""ProductTypesName""=@ProductTypesName
                where ""ProductTypesId""=@ProductTypesId 
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductTypesId", prod_type.ProductTypesId);
                    myCommand.Parameters.AddWithValue("@ProductTypesName", prod_type.ProductTypesName);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }
        }

        public async Task Delete(int id)
        {
            string query = @"
                delete from ""ProductTypes""
                where ""ProductTypesId""=@ProductTypesId 
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductTypesId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }
        }
    }
}
