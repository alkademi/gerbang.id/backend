﻿using Ensiklopedia46.Data;
using if3250_2022_05_gerbang_be.Data;
using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Npgsql;

namespace if3250_2022_05_gerbang_be.Services
{
    public class UserService : IUserService
    {
        private AppSettings _appSettings;
        private GerbangContext _db;
        private IConfiguration _configuration;
        private readonly UserManager<AspNetUser> _userManager;
        private readonly SignInManager<AspNetUser> _signInManager;
        private readonly RoleManager<AspNetRole> _roleManager;
        private readonly JWTHelperInt _jwtHelper;

        public UserService(
            IOptions<AppSettings> appSettings,
            GerbangContext db,
            IConfiguration configuration,
            UserManager<AspNetUser> userManager,
            SignInManager<AspNetUser> signInManager,
            RoleManager<AspNetRole> roleManager,
            JWTHelperInt jwtHelper)
        {
            _appSettings = appSettings.Value;
            _db = db;
            _configuration = configuration;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _jwtHelper = jwtHelper;
        }

        public async Task register(UserData user)
        {
            try
            {
                var isUserRoleExist = await _roleManager.RoleExistsAsync("User");

                if (!isUserRoleExist)
                {
                    var role = new AspNetRole();
                    role.Name = "User";
                    await _roleManager.CreateAsync(role);
                }

                if (user.Password.Equals(user.PasswordConfirmation))
                {
                    var newUser = new AspNetUser
                    {
                        FullName = user.FullName,
                        DateOfBirth = user.DateOfBirth,
                        Email = user.Email,
                        UserName = user.Username
                    };

                    var result = await _userManager.CreateAsync(newUser, user.Password);

                    if(!result.Succeeded)
                    {
                        throw new Exception($"Error : {result.Errors}");
                    }
                    await _userManager.AddToRoleAsync(newUser, "User");
                }
                else
                {
                    throw new Exception($"Error : Password does not match.");
                }
            }
            catch (DbUpdateException e)
            {
                throw new Exception($"Database Error: {e.Message}");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<AspNetUser> getAccountInfo(string user)
        {
            try
            {
                var gottenUser = await _userManager.FindByNameAsync(user);
                if(gottenUser != null)
                {
                    return gottenUser;
                }
                throw new Exception($"Error : User Not Found");
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public async Task<TokenResponse> authenticate(string username, string password, string ipAddress)
        {
            var user = await _userManager.FindByNameAsync(username);
            if(user == null || !await _userManager.CheckPasswordAsync(user, password))
            {
                return null;
            }

            RefreshToken refreshToken = _jwtHelper.generateRefreshToken(ipAddress);
            await _jwtHelper.removeOldRefreshToken(user);

            user.RefreshTokens.Add(refreshToken);

            // Save changes to DB
            _db.Update(user);
            await _db.SaveChangesAsync();

            return new TokenResponse
            {
                Username = username,
                Token = _jwtHelper.generateJwtToken(username),
                RefreshToken = refreshToken.Token
            };
        }

        public async Task<DataTable> GetAccountRole(string user)
        {
            string query = @"
            select  ""UserName"" as ""UserName"",
                    ""RoleId"" as ""RoleId""
            from ""AspNetUserRoles"",""AspNetUsers""
            where ""UserId""=""Id"" AND ""UserName"" = @Uname
        ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@Uname", user);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }
            return table;
        }

        public async Task<DataTable> GetUsername(string email)
        {
            string query = @"
            select  ""UserName"" as ""name""
            from ""AspNetUsers""
            where ""Email""= @QEmail
        ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@QEmail", email);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }
            return table;
        }
    }
}
