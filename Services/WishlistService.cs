﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using if3250_2022_05_gerbang_be.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace if3250_2022_05_gerbang_be.Services
{
    public class WishlistService : IWishlistService
    {
        private readonly IConfiguration _configuration;

        public WishlistService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<List<Wishlist>> Get()
        {
            string query = @"
                select ""id_wishlist"" as ""id_wishlist"",
                        ""id_product"" as ""id_product"",
                        ""username"" as ""username""
                from ""Wishlist""
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }

            var wishlist = (from rw in table.AsEnumerable()
                                select new Wishlist()
                                {
                                    id_wishlist = Convert.ToInt32(rw["id_wishlist"]),
                                    id_product = Convert.ToInt32(rw["id_product"]),
                                    username = Convert.ToString(rw["username"])
                                }).ToList();

            return wishlist;
        }

        public async Task<int> Post(Wishlist wishlist)
        {
            int id = -1;
            string query = @"
                insert into ""Wishlist""(""id_product"",""username"")
                values (@ProductId,@Username) RETURNING ""id_wishlist"";
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductId", wishlist.id_product);
                    myCommand.Parameters.AddWithValue("@Username", wishlist.username);
                    object returnObj = myCommand.ExecuteScalar();

                    if (returnObj != null)
                    {
                        int.TryParse(returnObj.ToString(), out id);
                    }
                }
            }
            return id;
        }

        public async Task Put(Wishlist wishlist)
        {
            string query = @"
                update ""Wishlist""
                set ""id_product""=@ProductId,
                    ""username""=@Username,
                where ""id_wishlist""=@WishlistId 
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@ProductId", wishlist.id_product);
                    myCommand.Parameters.AddWithValue("@Username", wishlist.username);
                    myCommand.Parameters.AddWithValue("@id_wishlist", wishlist.id_wishlist);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }
        }

        public async Task Delete(int id)
        {
            string query = @"
                delete from ""Wishlist""
                where ""id_wishlist""=@WishlistId 
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("DefaultConnection");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                await using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@WishlistId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                }
            }
        }
    }
}
