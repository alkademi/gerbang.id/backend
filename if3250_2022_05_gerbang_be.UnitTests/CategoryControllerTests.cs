﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Threading.Tasks;
using FluentAssertions;
using if3250_2022_05_gerbang_be.Controllers;
using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace if3250_2022_05_gerbang_be.UnitTests
{
    public class CategoryControllerTests
    {
        private readonly Mock<ICategoryService> categoryServiceStub = new();
        private readonly Random rand = new();

        // Consider there is product type with id equals to 1
        //private Category dummyCategory = new Category(){
        //    ProductTypesId = 1,
        //    CategoryName = Guid.NewGuid().ToString()
        //};

        // GET ALL
        [Fact]
        public async Task GetCategoriesAsync_WithExistingCategories_ReturnsAllCategories()
        {
            // Arrange
            var expectedCategories = new List<Category>()
            {
                CreateRandomCategory(),
                CreateRandomCategory(),
                CreateRandomCategory()
            };

            categoryServiceStub.Setup(repo => repo.Get())
                .ReturnsAsync(expectedCategories);

            var controller = new CategoryController(categoryServiceStub.Object);

            // Act
            var actualCategories = await controller.Get();

            // Assert
            actualCategories.Should().BeEquivalentTo(
                expectedCategories,
                options => options.ComparingByMembers<Category>()
                );
        }

        // GET ANY
        [Fact]
        public async Task GetCategoryAsync_WithExistingCategory_ReturnsExpectedCategory()
        {
            // Arrange
            var expectedCategory = new List<Category>()
            {
                CreateRandomCategory()
            };

            categoryServiceStub.Setup(repo => repo.Get(It.IsAny<int>()))
                .ReturnsAsync(expectedCategory);

            var controller = new CategoryController(categoryServiceStub.Object);

            // Act
            var result = await controller.Get(rand.Next(10));

            // Assert
            result.Should().BeEquivalentTo(
                expectedCategory,
                options => options.ComparingByMembers<Category>()
                );
        }

        //CREATE
        [Fact]
        public async Task CreateCategoryAsync_WithCategoryToCreate_ReturnsCreatedCategoryId()
        {
            // Arrange
            var itemToCreate = new Category()
            {
                ProductTypesId = rand.Next(10),
                CategoryName = Guid.NewGuid().ToString()
            };

            categoryServiceStub.Setup(repo => repo.Post(It.IsAny<Category>()))
                .ReturnsAsync(1);

            var controller = new CategoryController(categoryServiceStub.Object);

            // Act
            var result = await controller.Post(itemToCreate);

            // Assert
            itemToCreate.Should().BeEquivalentTo(
                result,
                options => options.ComparingByMembers<Category>().ExcludingMissingMembers());

            Assert.NotEqual(0, result.Value);
        }

        //CREATE
        [Fact]
        public async Task CreateCategoryAsync_WithFailToCreate_ReturnsDefaultErrorId()
        {
            // Arrange
            var itemToCreate = new Category()
            {
                ProductTypesId = rand.Next(10),
                CategoryName = Guid.NewGuid().ToString()
            };

            categoryServiceStub.Setup(repo => repo.Post(itemToCreate))
                .ReturnsAsync(-1);

            var controller = new CategoryController(categoryServiceStub.Object);

            // Act
            var result = await controller.Post(itemToCreate);

            // Assert
            itemToCreate.Should().BeEquivalentTo(
                result,
                options => options.ComparingByMembers<Category>().ExcludingMissingMembers());

            Assert.Equal(0, result.Value);
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        // UPDATE
        [Fact]
        public async Task UpdateCategoryAsync_WithExistingCategory_ReturnsOkResult()
        {
            // Arrange
            var itemToCreate = CreateRandomCategory();

            var controller = new CategoryController(categoryServiceStub.Object);

            // Act
            var result = await controller.Put(itemToCreate);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        // UPDATE
        [Fact]
        public async Task UpdateCategoryAsync_WithUnexistingCategory_ReturnsBadRequest()
        {
            // Arrange
            var itemToCreate = CreateRandomCategory();

            categoryServiceStub.Setup(repo => repo.Put(It.IsAny<Category>()))
                .ThrowsAsync(new Exception());

            var controller = new CategoryController(categoryServiceStub.Object);

            // Act
            var result = await controller.Put(itemToCreate);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        // DELETE
        [Fact]
        public async Task DeleteCategoryAsync_WithExistingCategory_ReturnsOkResult()
        {
            // Arrange
            var id = rand.Next(10);

            var controller = new CategoryController(categoryServiceStub.Object);

            // Act
            var result = await controller.Delete(id);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        // DELETE
        [Fact]
        public async Task DeleteCategoryAsync_WithUnexistingCategory_ReturnsBadRequest()
        {
            // Arrange
            var id = rand.Next(10);

            categoryServiceStub.Setup(repo => repo.Delete(It.IsAny<int>()))
                .ThrowsAsync(new Exception());

            var controller = new CategoryController(categoryServiceStub.Object);

            // Act
            var result = await controller.Delete(id);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        private Category CreateRandomCategory()
        {
            return new Category
            {
                CategoryId = rand.Next(10),
                ProductTypesId = rand.Next(10),
                CategoryName = Guid.NewGuid().ToString()
            };
        }
    }
}
