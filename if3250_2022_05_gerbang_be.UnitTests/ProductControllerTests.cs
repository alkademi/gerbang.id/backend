﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using if3250_2022_05_gerbang_be.Controllers;
using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using static if3250_2022_05_gerbang_be.Models.Product;
using static if3250_2022_05_gerbang_be.Services.ProductService;

namespace if3250_2022_05_gerbang_be.UnitTests
{
    public class ProductControllerTests
    {
        private readonly Mock<IProductService> productServiceStub = new();
        private readonly Random rand = new();

        // GET ALL
        [Fact]
        public async Task GetProductsAsync_WithExistingProducts_ReturnsAllProducts()
        {
            // Arrange
            var expectedProducts = new List<Product>()
            {
                CreateRandomProduct(),
                CreateRandomProduct(),
                CreateRandomProduct()
            };

            var prodAgeRestriction = Enum.GetValues(typeof(ages));
            var tabOptions = Enum.GetValues(typeof(tab_options));
            var sortOptions = Enum.GetValues(typeof(sort_options));

            productServiceStub.Setup(repo => repo.Get(
                It.IsAny<String>(),
                It.IsAny<ages>(),
                It.IsAny<int>(),
                It.IsAny<int>(),
                It.IsAny<tab_options>(),
                It.IsAny<sort_options>(),
                It.IsAny<bool>(),
                It.IsAny<int>()
                ))
                .ReturnsAsync(expectedProducts);

            var controller = new ProductController(productServiceStub.Object);

            // Act
            var actualProducts = await controller.Get(
                Guid.NewGuid().ToString(),
                (ages)prodAgeRestriction.GetValue(rand.Next(prodAgeRestriction.Length)),
                rand.Next(10),
                rand.Next(10),
                (tab_options)tabOptions.GetValue(rand.Next(tabOptions.Length)),
                (sort_options)sortOptions.GetValue(rand.Next(sortOptions.Length)),
                false,
                5);

            // Assert
            actualProducts.Should().BeEquivalentTo(
                expectedProducts,
                options => options.ComparingByMembers<Product>()
                );
        }

        // GET ANY
        [Fact]
        public async Task GetProductAsync_WithExistingProduct_ReturnsExpectedProduct()
        {
            // Arrange
            var expectedProduct = new List<Product>()
            {
                CreateRandomProduct()
            };

            productServiceStub.Setup(repo => repo.Get(It.IsAny<int>()))
                .ReturnsAsync(expectedProduct);

            var controller = new ProductController(productServiceStub.Object);

            // Act
            var result = await controller.Get(rand.Next(10));

            // Assert
            result.Should().BeEquivalentTo(
                expectedProduct,
                options => options.ComparingByMembers<Product>()
                );
        }

        //CREATE
        [Fact]
        public async Task CreateProductAsync_WithProductToCreate_ReturnsCreatedProductId()
        {
            // Arrange
            var prodSizeUnit = Enum.GetValues(typeof(size_units));
            var prodAgeRestriction = Enum.GetValues(typeof(ages));

            var itemToCreate = new Product
            {
                ProductName = Guid.NewGuid().ToString(),
                ProductTypesId = rand.Next(10),
                ProductCategoryId = rand.Next(10),
                ProductPublisher = Guid.NewGuid().ToString(),
                ProductRating = rand.Next(5),
                ProductSize = rand.Next(100),
                ProductSizeUnit = (size_units)prodSizeUnit.GetValue(rand.Next(prodSizeUnit.Length)),
                ProductAgeRestriction = (ages)prodAgeRestriction.GetValue(rand.Next(prodAgeRestriction.Length)),
                ProductDesc = Guid.NewGuid().ToString(),
                ProductLogo = Guid.NewGuid().ToString(),
                ProductLogoName = Guid.NewGuid().ToString(),
                ProductImages = new[] { Guid.NewGuid().ToString(), Guid.NewGuid().ToString() },
                ProductImagesName = new[] { Guid.NewGuid().ToString(), Guid.NewGuid().ToString() }
            };

            productServiceStub.Setup(repo => repo.Post(It.IsAny<Product>()))
                .ReturnsAsync(1);

            var controller = new ProductController(productServiceStub.Object);

            // Act
            var result = await controller.Post(itemToCreate);

            // Assert
            itemToCreate.Should().BeEquivalentTo(
                result,
                options => options.ComparingByMembers<Product>().ExcludingMissingMembers());

            Assert.NotEqual(0, result.Value);
        }

        //CREATE
        [Fact]
        public async Task CreateProductAsync_WithFailToCreate_ReturnsDefaultErrorId()
        {
            // Arrange
            var prodSizeUnit = Enum.GetValues(typeof(size_units));
            var prodAgeRestriction = Enum.GetValues(typeof(ages));

            var itemToCreate = new Product
            {
                ProductName = Guid.NewGuid().ToString(),
                ProductTypesId = rand.Next(10),
                ProductCategoryId = rand.Next(10),
                ProductPublisher = Guid.NewGuid().ToString(),
                ProductRating = rand.Next(5),
                ProductSize = rand.Next(100),
                ProductSizeUnit = (size_units)prodSizeUnit.GetValue(rand.Next(prodSizeUnit.Length)),
                ProductAgeRestriction = (ages)prodAgeRestriction.GetValue(rand.Next(prodAgeRestriction.Length)),
                ProductDesc = Guid.NewGuid().ToString(),
                ProductLogo = Guid.NewGuid().ToString(),
                ProductLogoName = Guid.NewGuid().ToString(),
                ProductImages = new[] { Guid.NewGuid().ToString(), Guid.NewGuid().ToString() },
                ProductImagesName = new[] { Guid.NewGuid().ToString(), Guid.NewGuid().ToString() }
            };

            productServiceStub.Setup(repo => repo.Post(itemToCreate))
                .ReturnsAsync(-1);

            var controller = new ProductController(productServiceStub.Object);

            // Act
            var result = await controller.Post(itemToCreate);

            // Assert
            itemToCreate.Should().BeEquivalentTo(
                result,
                options => options.ComparingByMembers<Product>().ExcludingMissingMembers());

            Assert.Equal(0, result.Value);
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        // UPDATE
        [Fact]
        public async Task UpdateProductAsync_WithExistingProduct_ReturnsOkResult()
        {
            // Arrange
            var itemToCreate = CreateRandomProduct();

            var controller = new ProductController(productServiceStub.Object);

            // Act
            var result = await controller.Put(itemToCreate);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        // UPDATE
        [Fact]
        public async Task UpdateProductAsync_WithUnexistingProduct_ReturnsBadRequest()
        {
            // Arrange
            var itemToCreate = CreateRandomProduct();

            productServiceStub.Setup(repo => repo.Put(It.IsAny<Product>()))
                .ThrowsAsync(new Exception());

            var controller = new ProductController(productServiceStub.Object);

            // Act
            var result = await controller.Put(itemToCreate);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        // DELETE
        [Fact]
        public async Task DeleteProductAsync_WithExistingProduct_ReturnsOkResult()
        {
            // Arrange
            var id = rand.Next(10);

            var controller = new ProductController(productServiceStub.Object);

            // Act
            var result = await controller.Delete(id);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        // DELETE
        [Fact]
        public async Task DeleteProductAsync_WithUnexistingProduct_ReturnsBadRequest()
        {
            // Arrange
            var id = rand.Next(10);

            productServiceStub.Setup(repo => repo.Delete(It.IsAny<int>()))
                .ThrowsAsync(new Exception());

            var controller = new ProductController(productServiceStub.Object);

            // Act
            var result = await controller.Delete(id);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        private Product CreateRandomProduct()
        {
            var prodSizeUnit = Enum.GetValues(typeof(size_units));
            var prodAgeRestriction = Enum.GetValues(typeof(ages));

            return new Product
            {
                ProductId = rand.Next(10),
                ProductName = Guid.NewGuid().ToString(),
                ProductTypesId = rand.Next(10),
                ProductCategoryId = rand.Next(10),
                ProductPublisher = Guid.NewGuid().ToString(),
                ProductRating = rand.Next(5),
                ProductSize = rand.Next(100),
                ProductSizeUnit = (size_units)prodSizeUnit.GetValue(rand.Next(prodSizeUnit.Length)),
                ProductAgeRestriction = (ages)prodAgeRestriction.GetValue(rand.Next(prodAgeRestriction.Length)),
                ProductDesc = Guid.NewGuid().ToString(),
                ProductLogo = Guid.NewGuid().ToString(),
                ProductLogoName = Guid.NewGuid().ToString(),
                ProductImages = new[] { Guid.NewGuid().ToString(), Guid.NewGuid().ToString() },
                ProductImagesName = new[] { Guid.NewGuid().ToString(), Guid.NewGuid().ToString() }
            };
        }
    }
}
