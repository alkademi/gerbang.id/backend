﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using if3250_2022_05_gerbang_be.Controllers;
using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using static if3250_2022_05_gerbang_be.Services.ProductReviewsService;

namespace if3250_2022_05_gerbang_be.UnitTests
{
    public class ProductReviewsControllerTests
    {
        private readonly Mock<IProductReviewsService> productReviewsService = new();
        private readonly Random rand = new();

        // GET ALL
        [Fact]
        public async Task GetProductReviewsAsync_WithExistingProductReviews_ReturnsAllProductReviews()
        {
            // Arrange
            var expectedProductReviews = new List<ProductReviews>()
            {
                CreateRandomProductReview(),
                CreateRandomProductReview(),
                CreateRandomProductReview()
            };
 
            productReviewsService.Setup(repo => repo.Get(
                It.IsAny<int?>(),
                It.IsAny<int?>(),
                It.IsAny<int?>(),
                It.IsAny<int?>(),
                It.IsAny<DateTime?>(),
                It.IsAny<string>(),
                It.IsAny<review_sort_opt?>()
                ))
                .ReturnsAsync(expectedProductReviews);

            var controller = new ProductReviewsController(productReviewsService.Object);

            var sort_by = Enum.GetValues(typeof(review_sort_opt));

            // Act
            var actualProducts = await controller.Get(
                rand.Next(10),
                rand.Next(10),
                rand.Next(10),
                rand.Next(5),
                DateTime.UtcNow,
                Guid.NewGuid().ToString(),
                (review_sort_opt)sort_by.GetValue(rand.Next(sort_by.Length))
                );

            // Assert
            actualProducts.Should().BeEquivalentTo(
                expectedProductReviews,
                options => options.ComparingByMembers<ProductReviews>()
                );
        }

        // GET ANY
        [Fact]
        public async Task GetProductReviewAsync_WithExistingProductReview_ReturnsExpectedProductReview()
        {
            // Arrange
            var expectedProductReviews = new List<ProductReviews>()
            {
                CreateRandomProductReview()
            };

            productReviewsService.Setup(repo => repo.Get(It.IsAny<int>()))
                .ReturnsAsync(expectedProductReviews);

            var controller = new ProductReviewsController(productReviewsService.Object);

            // Act
            var result = await controller.Get(rand.Next(10));

            // Assert
            result.Should().BeEquivalentTo(
                expectedProductReviews,
                options => options.ComparingByMembers<ProductReviews>()
                );
        }

        //CREATE
        [Fact]
        public async Task CreateProductReviewAsync_WithProductReviewToCreate_ReturnsCreatedProductReviewId()
        {
            // Arrange
            var itemToCreate = new ProductReviews
            {
                ProductId = rand.Next(10),
                UserId = rand.Next(10),
                Stars = rand.Next(10),
                ReviewDate = DateTime.Now,
                ReviewText = Guid.NewGuid().ToString()
            };

            productReviewsService.Setup(repo => repo.Post(It.IsAny<ProductReviews>()))
                .ReturnsAsync(1);

            var controller = new ProductReviewsController(productReviewsService.Object);

            // Act
            var result = await controller.Post(itemToCreate);

            // Assert
            itemToCreate.Should().BeEquivalentTo(
                result,
                options => options.ComparingByMembers<ProductReviews>().ExcludingMissingMembers());

            Assert.NotEqual(0, result.Value);
        }

        //CREATE
        [Fact]
        public async Task CreateProductReviewAsync_WithFailToCreate_ReturnsDefaultErrorId()
        {
            // Arrange
            var itemToCreate = new ProductReviews
            {
                ProductId = rand.Next(10),
                UserId = rand.Next(10),
                Stars = rand.Next(10),
                ReviewDate = DateTime.Now,
                ReviewText = Guid.NewGuid().ToString()
            };

            productReviewsService.Setup(repo => repo.Post(itemToCreate))
                .ReturnsAsync(-1);

            var controller = new ProductReviewsController(productReviewsService.Object);

            // Act
            var result = await controller.Post(itemToCreate);

            // Assert
            itemToCreate.Should().BeEquivalentTo(
                result,
                options => options.ComparingByMembers<ProductReviews>().ExcludingMissingMembers());

            Assert.Equal(0, result.Value);
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        // UPDATE
        [Fact]
        public async Task UpdateProductReviewAsync_WithExistingProductReview_ReturnsOkResult()
        {
            // Arrange
            var controller = new ProductReviewsController(productReviewsService.Object);

            // Act
            var result = await controller.Put(
                rand.Next(10),
                rand.Next(10),
                rand.Next(10),
                rand.Next(5),
                DateTime.UtcNow,
                Guid.NewGuid().ToString()
                );

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        // UPDATE
        [Fact]
        public async Task UpdateProductReviewAsync_WithUnexistingProductReview_ReturnsBadRequest()
        {
            // Arrange
            productReviewsService.Setup(repo => repo.Put(
                It.IsAny<int>(),
                It.IsAny<int?>(),
                It.IsAny<int?>(),
                It.IsAny<int>(),
                It.IsAny<DateTime?>(),
                It.IsAny<string>()
                ))
                .ThrowsAsync(new Exception());

            var controller = new ProductReviewsController(productReviewsService.Object);

            // Act
            var result = await controller.Put(
                rand.Next(10),
                rand.Next(10),
                rand.Next(10),
                rand.Next(5),
                DateTime.UtcNow,
                Guid.NewGuid().ToString()
                );

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        // DELETE
        [Fact]
        public async Task DeleteProductReviewAsync_WithExistingProductReview_ReturnsOkResult()
        {
            // Arrange
            var id = rand.Next(10);

            var controller = new ProductReviewsController(productReviewsService.Object);

            // Act
            var result = await controller.Delete(id);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        // DELETE
        [Fact]
        public async Task DeleteProductReviewAsync_WithUnexistingProductReview_ReturnsBadRequest()
        {
            // Arrange
            var id = rand.Next(10);

            productReviewsService.Setup(repo => repo.Delete(It.IsAny<int>()))
                .ThrowsAsync(new Exception());

            var controller = new ProductReviewsController(productReviewsService.Object);

            // Act
            var result = await controller.Delete(id);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        private ProductReviews CreateRandomProductReview()
        {
            return new ProductReviews
            {
                ReviewId = rand.Next(10),
                ProductId = rand.Next(10),
                UserId = rand.Next(10),
                Stars = rand.Next(10),
                ReviewDate = DateTime.Now,
                ReviewText = Guid.NewGuid().ToString()
            };
        }
    }
}
