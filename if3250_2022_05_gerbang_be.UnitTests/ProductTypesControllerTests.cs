﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using if3250_2022_05_gerbang_be.Controllers;
using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace if3250_2022_05_gerbang_be.UnitTests
{
    public class ProductTypesControllerTests
    {
        private readonly Mock<IProductTypesService> productTypesService = new();
        private readonly Random rand = new();

        // GET ALL
        [Fact]
        public async Task GetProductTypesAsync_WithExistingProductTypes_ReturnsAllProductTypes()
        {
            // Arrange
            var expectedProductTypes = new List<ProductTypes>()
            {
                CreateRandomProductType(),
                CreateRandomProductType(),
                CreateRandomProductType()
            };

            productTypesService.Setup(repo => repo.Get())
                .ReturnsAsync(expectedProductTypes);

            var controller = new ProductTypesController(productTypesService.Object);

            // Act
            var actualProductTypes = await controller.Get();

            // Assert
            actualProductTypes.Should().BeEquivalentTo(
                expectedProductTypes,
                options => options.ComparingByMembers<ProductTypes>()
                );
        }

        //CREATE
        [Fact]
        public async Task CreateProductTypesAsync_WithProductTypesToCreate_ReturnsCreatedProductTypesId()
        {
            // Arrange
            var itemToCreate = new ProductTypes
            {
                ProductTypesName = Guid.NewGuid().ToString()
            };

            productTypesService.Setup(repo => repo.Post(It.IsAny<ProductTypes>()))
                .ReturnsAsync(1);

            var controller = new ProductTypesController(productTypesService.Object);

            // Act
            var result = await controller.Post(itemToCreate);

            // Assert
            itemToCreate.Should().BeEquivalentTo(
                result,
                options => options.ComparingByMembers<ProductTypes>().ExcludingMissingMembers());

            Assert.NotEqual(0, result.Value);
        }

        //CREATE
        [Fact]
        public async Task CreateProductTypesAsync_WithFailToCreate_ReturnsDefaultErrorId()
        {
            // Arrange
            var itemToCreate = new ProductTypes
            {
                ProductTypesName = Guid.NewGuid().ToString()
            };

            productTypesService.Setup(repo => repo.Post(It.IsAny<ProductTypes>()))
                .ReturnsAsync(-1);

            var controller = new ProductTypesController(productTypesService.Object);

            // Act
            var result = await controller.Post(itemToCreate);

            // Assert
            itemToCreate.Should().BeEquivalentTo(
                result,
                options => options.ComparingByMembers<ProductTypes>().ExcludingMissingMembers());

            Assert.Equal(0, result.Value);
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        // UPDATE
        [Fact]
        public async Task UpdateProductTypesAsync_WithExistingProductTypes_ReturnsOkResult()
        {
            // Arrange
            var itemToCreate = CreateRandomProductType();

            var controller = new ProductTypesController(productTypesService.Object);

            // Act
            var result = await controller.Put(itemToCreate);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        // UPDATE
        [Fact]
        public async Task UpdateProductTypesAsync_WithUnexistingProductTypes_ReturnsBadRequest()
        {
            // Arrange
            var itemToCreate = CreateRandomProductType();

            productTypesService.Setup(repo => repo.Put(It.IsAny<ProductTypes>()))
                .ThrowsAsync(new Exception());

            var controller = new ProductTypesController(productTypesService.Object);

            // Act
            var result = await controller.Put(itemToCreate);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        // DELETE
        [Fact]
        public async Task DeleteProductTypesAsync_WithExistingProductTypes_ReturnsOkResult()
        {
            // Arrange
            var id = rand.Next(10);

            var controller = new ProductTypesController(productTypesService.Object);

            // Act
            var result = await controller.Delete(id);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        // DELETE
        [Fact]
        public async Task DeleteProductTypesAsync_WithUnexistingProductTypes_ReturnsBadRequest()
        {
            // Arrange
            var id = rand.Next(10);

            productTypesService.Setup(repo => repo.Delete(It.IsAny<int>()))
                .ThrowsAsync(new Exception());

            var controller = new ProductTypesController(productTypesService.Object);

            // Act
            var result = await controller.Delete(id);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        private ProductTypes CreateRandomProductType()
        {
            return new ProductTypes
            {
                ProductTypesId = rand.Next(10),
                ProductTypesName = Guid.NewGuid().ToString()
            };
        }
    }
}
