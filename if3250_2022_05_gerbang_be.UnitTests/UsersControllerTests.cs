﻿using System;
using System.Threading.Tasks;
using if3250_2022_05_gerbang_be.Controllers;
using if3250_2022_05_gerbang_be.Data;
using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace if3250_2022_05_gerbang_be.UnitTests
{
    public class UsersControllerTests
    {

        private readonly Mock<IUserService> userService = new();
        private readonly Random rand = new();

        [Fact]
        public async Task RegisterAsync_WithValidData_ReturnsOkResult()
        {
            // Arrange
            var itemToCreate = CreateRandomUser();
            var controller = new UsersController(userService.Object);

            // Act
            var result = await controller.Register(itemToCreate);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task RegisterAsync_WithInvalidData_ReturnsBadRequest()
        {
            // Arrange
            var itemToCreate = CreateRandomUser();

            userService.Setup(repo => repo.register(It.IsAny<UserData>()))
                .ThrowsAsync(new Exception());

            var controller = new UsersController(userService.Object);

            // Act
            var result = await controller.Register(itemToCreate);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        //[Fact]
        //public async Task AuthenticateAsync_WithValidAccount_ReturnsOkResult()
        //{
        //    // Arrange
        //    var itemToCreate = CreateRandomUser();

        //    userService.Setup(repo => repo.authenticate(
        //        It.IsAny<String>(),
        //        It.IsAny<String>(),
        //        It.IsAny<String>()))
        //        .ReturnsAsync(CreateRandomTokenResponse());

        //    userService.Setup(repo => repo.ipAddress(
        //        )
        //        .ReturnsAsync(CreateRandomTokenResponse());

        //    var controller = new UsersController(userService.Object);

        //    // Act
        //    var result = await controller.Authentication(CreateRandomUserLoginData());

        //    // Assert
        //    Assert.IsType<OkObjectResult>(result);
        //}

        //[Fact]
        //public async Task AuthenticateAsync_WithInvalidAccount_ReturnsBadRequest()
        //{
        //    // Arrange
        //    var itemToCreate = CreateRandomUser();

        //    userService.Setup(repo => repo.authenticate(
        //        It.IsAny<String>(),
        //        It.IsAny<String>(),
        //        It.IsAny<String>()))
        //        .ReturnsAsync((TokenResponse) null);

        //    var controller = new UsersController(userService.Object);

        //    // Act
        //    var result = await controller.Authentication(CreateRandomUserLoginData());

        //    // Assert
        //    Assert.IsType<BadRequestObjectResult>(result);
        //}

        private UserData CreateRandomUser()
        {
            return new UserData
            {
                FullName = Guid.NewGuid().ToString(),
                DateOfBirth = DateTime.UtcNow,
                Email = Guid.NewGuid().ToString(),
                Username = Guid.NewGuid().ToString(),
                Password = Guid.NewGuid().ToString(),
                PasswordConfirmation = Guid.NewGuid().ToString()
            };
        }

        private TokenResponse CreateRandomTokenResponse()
        {
            return new TokenResponse
            {
                Username = Guid.NewGuid().ToString(),
                Token = Guid.NewGuid().ToString(),
                RefreshToken = Guid.NewGuid().ToString()
            };
        }

        private UserLoginData CreateRandomUserLoginData()
        {
            return new UserLoginData
            {
                Username = Guid.NewGuid().ToString(),
                Password = Guid.NewGuid().ToString()
            };
        }
    }
}
