﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using if3250_2022_05_gerbang_be.Controllers;
using if3250_2022_05_gerbang_be.Models;
using if3250_2022_05_gerbang_be.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace if3250_2022_05_gerbang_be.UnitTests
{
    public class WishlistControllerTests
    {
        private readonly Mock<IWishlistService> wishlistService = new();
        private readonly Random rand = new();

        // GET ALL
        [Fact]
        public async Task GetWishlistAsync_WithExistingWishlist_ReturnsAllWishlist()
        {
            // Arrange
            var expectedWishlist = new List<Wishlist>()
            {
                CreateRandomSingleWishlist(),
                CreateRandomSingleWishlist(),
                CreateRandomSingleWishlist()
            };

            wishlistService.Setup(repo => repo.Get())
                .ReturnsAsync(expectedWishlist);

            var controller = new WishlistController(wishlistService.Object);

            // Act
            var actualWishlist = await controller.Get();

            // Assert
            actualWishlist.Should().BeEquivalentTo(
                expectedWishlist,
                options => options.ComparingByMembers<Wishlist>()
                );
        }

        //CREATE
        [Fact]
        public async Task CreateWishlistAsync_WithWishlistToCreate_ReturnsCreatedWishlistId()
        {
            // Arrange
            var itemToCreate = new Wishlist
            {
                id_product = rand.Next(10),
                username = Guid.NewGuid().ToString()
            };

            wishlistService.Setup(repo => repo.Post(It.IsAny<Wishlist>()))
                .ReturnsAsync(1);

            var controller = new WishlistController(wishlistService.Object);

            // Act
            var result = await controller.Post(itemToCreate);

            // Assert
            itemToCreate.Should().BeEquivalentTo(
                result,
                options => options.ComparingByMembers<Wishlist>().ExcludingMissingMembers());

            Assert.NotEqual(0, result.Value);
        }

        //CREATE
        [Fact]
        public async Task CreateWishlistAsync_WithFailToCreate_ReturnsDefaultErrorId()
        {
            // Arrange
            var itemToCreate = new Wishlist
            {
                id_product = rand.Next(10),
                username = Guid.NewGuid().ToString()
            };

            wishlistService.Setup(repo => repo.Post(It.IsAny<Wishlist>()))
                .ReturnsAsync(-1);

            var controller = new WishlistController(wishlistService.Object);

            // Act
            var result = await controller.Post(itemToCreate);

            // Assert
            itemToCreate.Should().BeEquivalentTo(
                result,
                options => options.ComparingByMembers<Wishlist>().ExcludingMissingMembers());

            Assert.Equal(0, result.Value);
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        // UPDATE
        [Fact]
        public async Task UpdateWishlistAsync_WithExistingWishlist_ReturnsOkResult()
        {
            // Arrange
            var itemToCreate = CreateRandomSingleWishlist();

            var controller = new WishlistController(wishlistService.Object);

            // Act
            var result = await controller.Put(itemToCreate);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        // UPDATE
        [Fact]
        public async Task UpdateWishlistAsync_WithUnexistingWishlist_ReturnsBadRequest()
        {
            // Arrange
            var itemToCreate = CreateRandomSingleWishlist();

            wishlistService.Setup(repo => repo.Put(It.IsAny<Wishlist>()))
                .ThrowsAsync(new Exception());

            var controller = new WishlistController(wishlistService.Object);

            // Act
            var result = await controller.Put(itemToCreate);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        // DELETE
        [Fact]
        public async Task DeleteWishlistAsync_WithExistingWishlist_ReturnsOkResult()
        {
            // Arrange
            var id = rand.Next(10);

            var controller = new WishlistController(wishlistService.Object);

            // Act
            var result = await controller.Delete(id);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        // DELETE
        [Fact]
        public async Task DeleteWishlistAsync_WithUnexistingWishlist_ReturnsBadRequest()
        {
            // Arrange
            var id = rand.Next(10);

            wishlistService.Setup(repo => repo.Delete(It.IsAny<int>()))
                .ThrowsAsync(new Exception());

            var controller = new WishlistController(wishlistService.Object);

            // Act
            var result = await controller.Delete(id);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        private Wishlist CreateRandomSingleWishlist()
        {
            return new Wishlist
            {
                id_wishlist = rand.Next(10),
                id_product = rand.Next(10),
                username = Guid.NewGuid().ToString()
            };
        }
    }
}
